#using packages, command+return to run code
import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from IPython.display import display
from IPython.display import Image
%matplotlib inline
dfAccused=pd.read_csv('Witchcraftsurvey_csv/WDB_Accused.csv')
sns.countplot(x='SocioecStatus', data=dfAccused)
#white magic:
#merge WDB_Accused.csv +'AccusedRef' WDB_Case.csv 'CaseRef'+ WDB_WhiteMagic.csv
#'WhiteMagic_Type' in WDB_WhiteMagic.csv
#'SocioecStatus' in WDB_Accused.csv
dfCase=pd.read_csv('Witchcraftsurvey_csv/WDB_Case.csv')
dfWhiteM=pd.read_csv('Witchcraftsurvey_csv/WDB_WhiteMagic.csv')
dftool=pd.merge(dfCase, dfAccused, on='AccusedRef')
dfSWM=pd.merge(dftool, dfWhiteM, on='CaseRef')
#By merging these table, now SocioecStatus is in the same table as WhiteMagic_Type
dfSWM[['WhiteMagic_Type','SocioecStatus']]
SWMcp=sns.countplot(x='WhiteMagic_Type',hue='SocioecStatus', data=dfSWM)
SWMcp.set_xticklabels(SWMcp.get_xticklabels(), fontsize=10,rotation=40, ha="right")
plt.legend(loc='upper right')
plt.tight_layout()
plt.show()
SWMcp=sns.countplot(x='SocioecStatus', data=dfSWM)

dfWeaMod=pd.read_csv('Witchcraftsurvey_csv/WDB_WeatherModification.csv')
dftool2=pd.merge(dfWeaMod, dfWhiteM, on='CaseRef')
dftool2[['CaseRef','WeatherModification_Type','WhiteMagic_Type']]
#一个人可以被质控有多重魔法，这可咋整

dfWea=pd.merge(dfWeaMod, dftool, on='CaseRef')
Weathersc=sns.countplot(x='SocioecStatus', data=dfWea)
plt.show()
#只有6 out of 25的被指控weather modification的巫师有social status属性

dfShaCha=pd.read_csv('Witchcraftsurvey_csv/WDB_ShapeChanging.csv')
dfShapeCha=pd.merge(dfShaCha, dftool, on='CaseRef')
Shapesc=sns.countplot(x='SocioecStatus', data=dfShapeCha)
plt.show()

dfMusIns=pd.read_csv('Witchcraftsurvey_csv/WDB_MusicalInstrument.csv')
dfMusic=pd.merge(dfMusIns, dftool, on='CaseRef')
Musicsc=sns.countplot(x='SocioecStatus', data=dfMusic)
plt.show()




#——————————————————————分割线——————————WDB_DevilAppearance—————————————————
#WDB_DevilAppearance.csv devil的存在是否属于magic的一种
dfDevApp=pd.read_csv('Witchcraftsurvey_csv/WDB_DevilAppearance.csv')
dfDevil=pd.merge(dfDevApp, dftool, on='CaseRef')
DevilAppear=sns.countplot(x='SocioecStatus', data=dfDevil)
plt.show()


#Devil中各个social class的人数数量
dfDevSoc=dfDevil[['SocioecStatus','DevilRef']]
dfDevil_num=dfDevSoc.groupby('SocioecStatus').count()

#各个social class的人数总量
dfTotal=dftool[['SocioecStatus','CaseRef']]
dfTotal_num=dfTotal.groupby('SocioecStatus').count()

#合成一张表
dfNumDevTotal=pd.concat([dfDevil_num, dfTotal_num], axis=1,sort=True)
print(dfNumDevTotal)

dfDevPro=dfNumDevTotal.eval('proportion = DevilRef / CaseRef')
print(dfDevPro)

#将index转换为列
#并更改index顺序使其按地位顺序排序
dfDevPro['SocioecStatus'] = dfDevPro.index
dfDevPro=dfDevPro.reindex(index=['Landless','Very Poor','Lower','Middling','Upper','Nobility/Chiefs','Lairds/Baron'])
print(dfDevPro)

#去除nan所在行
devNotNull=dfDevPro.dropna()
print(devNotNull)

#pointplot-恶魔出现
sns.catplot(x='SocioecStatus', y="proportion", kind = "point", data=devNotNull)





#———————————————————分割线———————WDB_Elf_FairyElements.csv—————————————
dfElfFai=pd.read_csv('Witchcraftsurvey_csv/WDB_Elf_FairyElements.csv')
dfElf=pd.merge(dfElfFai, dftool, on='CaseRef')
ElfElements=sns.countplot(x='SocioecStatus', data=dfElf)
plt.show()
dfElfSoc=dfElf[['SocioecStatus','ElfFairyRef']]
dfElf_num=dfElfSoc.groupby('SocioecStatus').count()
dfNumElfTotal=pd.concat([dfElf_num, dfTotal_num], axis=1,sort=True)
dfElfPro=dfNumElfTotal.eval('proportion = ElfFairyRef / CaseRef')
dfElfPro['SocioecStatus'] = dfElfPro.index
dfElfPro=dfElfPro.reindex(index=['Landless','Very Poor','Lower','Middling','Upper','Nobility/Chiefs','Lairds/Baron'])
elfNotNull=dfElfPro.dropna()
print(elfNotNull)
sns.catplot(x='SocioecStatus', y="proportion", kind = "point", data=elfNotNull)

#________组合________________
dfElfSoc=dfElf[['SocioecStatus','ElfFairyRef']]
dfElf_num=dfElfSoc.groupby('SocioecStatus').count()
dfNumElfTotal=pd.concat([dfElf_num,dfDevil_num,dfTotal_num], axis=1,sort=True)
dfElfPro=dfNumElfTotal.eval('ElfPro = ElfFairyRef / CaseRef')
dfElfPro=dfElfPro.eval('DevPro = DevilRef / CaseRef')
dfElfPro['SocioecStatus'] = dfElfPro.index
dfElfPro=dfElfPro.reindex(index=['Landless','Very Poor','Lower','Middling','Upper','Nobility/Chiefs','Lairds/Baron'])
#elfNotNull=dfElfPro.dropna()
#print(elfNotNull)
print(dfElfPro)
#不对，应改成proportion和proportion type
#MagicType,Propotion,SocialClass三列
#{'MagicType': MagicT(列名),'Proportion':值, 'SocialClass':行名}

dfNewPro=dfElfPro[['ElfPro','DevPro','SocioecStatus']]
print(dfNewPro)

#——————————————————rearrange the dataframe————————————————————————
df_empty = pd.DataFrame(columns=['SocialClass','MagicType', 'Proportion'])
def SetDF(df,SocClass, ColName, Pro):
    newdf=df.append({'SocialClass':SocClass,'MagicType':ColName,'Proportion':Pro},ignore_index=True)
    return newdf
#df df_empty
#ColName

for index, row in dfNewPro.iterrows():
    df_empty=SetDF(df_empty,index,'Elf Fairy Element',row['ElfPro'])
    df_empty=SetDF(df_empty,index,'Devil Appearance',row['DevPro'])


#dfTestNoNan=df_empty.dropna()
#print(dfTestNoNan)
#graphtest=sns.catplot(x="SocialClass", y="Proportion", hue="MagicType",data=dfTestNoNan)

dfZerotoNan = df_empty.fillna(0)
print(dfZerotoNan)
#——————————draw point plot_____________
graphtest=sns.catplot(x="SocialClass", y="Proportion", hue="MagicType",kind="point",data=dfZerotoNan)


#------扩展到所有魔法类别---------------------
#weather modification:
#dfWea=pd.merge(dfWeaMod, dftool, on='CaseRef')
#Weathersc=sns.countplot(x='SocioecStatus', data=dfWea)
#Shape Change
#dfShapeCha=pd.merge(dfShaCha, dftool, on='CaseRef')
#Shapesc=sns.countplot(x='SocioecStatus', data=dfShapeCha)
#Musical Instrument
#dfMusic=pd.merge(dfMusIns, dftool, on='CaseRef')



#加proportion,生成初始图表
dfCase=pd.read_csv('Witchcraftsurvey_csv/WDB_Case.csv')
dfWhiteM=pd.read_csv('Witchcraftsurvey_csv/WDB_WhiteMagic.csv')
dfWeaMod=pd.read_csv('Witchcraftsurvey_csv/WDB_WeatherModification.csv')
dfShaCha=pd.read_csv('Witchcraftsurvey_csv/WDB_ShapeChanging.csv')
dfMusIns=pd.read_csv('Witchcraftsurvey_csv/WDB_MusicalInstrument.csv')
dfDevApp=pd.read_csv('Witchcraftsurvey_csv/WDB_DevilAppearance.csv')
dfElfFai=pd.read_csv('Witchcraftsurvey_csv/WDB_Elf_FairyElements.csv')

dftool=pd.merge(dfCase, dfAccused, on='AccusedRef')
#dftool=dftool[['CaseRef','SocioecStatus']]
#dftool = dftool.dropna()
#wrong
dfWeatherMod=pd.merge(dfWeaMod, dftool, on='CaseRef',how='outer')
dfShapeChage=pd.merge(dfShaCha, dfWeatherMod, on='CaseRef',how='outer')
dfMusicalIns=pd.merge(dfMusIns, dfShapeChage, on='CaseRef',how='outer')
dfElfFairy=pd.merge(dfElfFai, dfMusicalIns, on='CaseRef',how='outer')
dfDevilAppea=pd.merge(dfDevApp, dfElfFairy, on='CaseRef',how='outer')
dfWhiteMagic=pd.merge(dfWhiteM, dfDevilAppea, on='CaseRef',how='outer')
dforigin=dfWhiteMagic[['SocioecStatus','ShapeChangingRef','WeatherModificationRef'
,'MusicalInstrumentRef','WhiteMagicRef','DevilRef','ElfFairyRef']]
#drop nan in SocioeStatus Column
dforigin=dforigin.dropna(subset = ['SocioecStatus'])
#drop 后面属性全是nan的行-即：至少有2个非空值的行
dforigin=dforigin.dropna(thresh=2)
print(dforigin)


#各个social class的人数总量
dfTotal=dftool[['SocioecStatus','CaseRef']]
dfTotal_num=dfTotal.groupby('SocioecStatus').count()

dfDevil_num=dforigin[['SocioecStatus','DevilRef']].groupby('SocioecStatus').count()
dfWhitM_num=dforigin[['SocioecStatus','WhiteMagicRef']].groupby('SocioecStatus').count()
dfElfF_num=dforigin[['SocioecStatus','ElfFairyRef']].groupby('SocioecStatus').count()
dfMusi_num=dforigin[['SocioecStatus','MusicalInstrumentRef']].groupby('SocioecStatus').count()
dfShap_num=dforigin[['SocioecStatus','ShapeChangingRef']].groupby('SocioecStatus').count()
dfWeaMod_num=dforigin[['SocioecStatus','WeatherModificationRef']].groupby('SocioecStatus').count()

dfDevTotal=pd.concat([dfDevil_num, dfTotal_num], axis=1,sort=True)
dfWhiTotal=pd.concat([dfWhitM_num, dfDevTotal], axis=1,sort=True)
dfElfFTotal=pd.concat([dfElfF_num, dfWhiTotal], axis=1,sort=True)
dfMusiTotal=pd.concat([dfMusi_num, dfElfFTotal], axis=1,sort=True)
dfShapTotal=pd.concat([dfShap_num, dfMusiTotal], axis=1,sort=True)
dfTotal=pd.concat([dfWeaMod_num, dfShapTotal], axis=1,sort=True)
#表格：各个阶层在各个魔法上的人数
print(dfTotal)

dftd=dfTotal.eval('DevPro = DevilRef / CaseRef')
dfwm=dftd.eval('WhiPro = WhiteMagicRef / CaseRef')
dfef=dfwm.eval('ElfPro = ElfFairyRef/ CaseRef')
dfmt=dfef.eval('MusPro = MusicalInstrumentRef / CaseRef')
dfst=dfmt.eval('ShaPro =ShapeChangingRef / CaseRef')
dfpropor=dfst.eval('WeaPro =WeatherModificationRef / CaseRef')
print(dfpropor)

#set index order and set change index to a column
dfpropor['SocioecStatus'] = dfpropor.index
dfpropor=dfpropor.reindex(index=['Landless','Very Poor','Lower','Middling','Upper','Nobility/Chiefs','Lairds/Baron'])
print(dfpropor)

dfProportion=dfpropor[['DevPro','WhiPro','ElfPro','MusPro','ShaPro','WeaPro']]
print(dfProportion)

#rearrange the dataframe
df_reshapePro= pd.DataFrame(columns=['SocialClass','MagicType', 'Proportion'])
print(df_reshapePro)
def SetDF(df,SocClass, ColName, Pro):
    newdf=df.append({'SocialClass':SocClass,'MagicType':ColName,'Proportion':Pro},ignore_index=True)
    return newdf

for index, row in dfProportion.iterrows():
    df_reshapePro=SetDF(df_reshapePro,index,'Devil Appearance',row['DevPro'])
    df_reshapePro=SetDF(df_reshapePro,index,'White Magic',row['WhiPro'])
    df_reshapePro=SetDF(df_reshapePro,index,'Elf Fairy Element',row['ElfPro'])
    df_reshapePro=SetDF(df_reshapePro,index,'Musical Instrument',row['MusPro'])
    df_reshapePro=SetDF(df_reshapePro,index,'Shape Changing',row['ShaPro'])
    df_reshapePro=SetDF(df_reshapePro,index,'Weather Modification',row['WeaPro'])
dfZeroNan =df_reshapePro.fillna(0)
print(dfZeroNan)

#graph=sns.catplot(x="SocialClass", y="Proportion", hue="MagicType",kind="point",data=dfZeroNan,dodge=True)
#graph1.set_xticklabels(graph1.get_xticklabels(), fontsize=10,rotation=40, ha="right")
#graph.set_xticklabels(graph.get_xticklabels(),fontsize=6)
graph2=sns.pointplot(x="SocialClass", y="Proportion", hue="MagicType",data=dfZeroNan)
graph2.set_xticklabels(graph2.get_xticklabels(),rotation=30,fontsize=10)#解决xticklabel overlap的问题
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)#将legend移出plot
plt.show


graph3=sns.barplot(x="SocialClass", y="Proportion", hue="MagicType", data=dfZeroNan)
graph3.set_xticklabels(graph3.get_xticklabels(),rotation=10,fontsize=10)#解决xticklabel overlap的问题
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)#将legend移出plot
plt.show


indexa=['Landless','Very Poor','Lower','Middling','Upper','Nobility/Chiefs','Lairds/Baron']
dfZeroNan1 = dfZeroNan.pivot("SocialClass","MagicType", "Proportion")
dfZeroNan1 = dfZeroNan1.reindex(index=indexa)#将Index按地位顺序排序
heatMap1 = sns.heatmap(dfZeroNan1,
    square=True, # copy:make cells square
    cbar_kws={'fraction' : 0.01}, #  copy:shrink colour bar
    cmap='OrRd', #  copy:use orange/red colour map
    linewidth=1)
heatMap1.set_xticklabels(heatMap1.get_xticklabels(), rotation=45,fontsize=8,horizontalalignment='right')
heatMap1.set_yticklabels(heatMap1.get_yticklabels(), rotation=45,fontsize=8, horizontalalignment='right')

#
temp=[]
for row in dfProportion.iterrows():
    index, data = row
    temp.append((data*10).tolist())
print(temp[3])


#每种阶层一个python matplolib polar chart->设置树桩->在plot中定位
import plotly.graph_objects as go

fig = go.Figure(go.Barpolar(
    r=temp[3],
    theta=[65, 15, 210, 110, 180, 270],
    width=[25,25,25,25,25,25],
    marker_color=["#E4FF87", '#228B22', '#709BFF', '#FF6347',  '#FFD700', '#B6FFB4'],
    marker_line_color="black",
    marker_line_width=2,
    opacity=0.8
))

fig.update_layout(
    template=None,
    polar = dict(
        radialaxis = dict(range=[0, 5], showticklabels=False, ticks=''),
        angularaxis = dict(showticklabels=False, ticks='')
    )
)
fig.show()









#WDB_OtherCharges.csv 其他指控
